from flask_marshmallow import Marshmallow

from models import Hotels

ma = Marshmallow()


class HotelSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Hotels
    id = ma.auto_field()
    name = ma.auto_field()
    price = ma.auto_field()
    location = ma.auto_field()
    rate = ma.auto_field()
    breakfast = ma.auto_field()


hotel_schema = HotelSchema()
hotels_schema = HotelSchema(many=True)
