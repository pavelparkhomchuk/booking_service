from flask import Flask
from flask_restful import Resource, Api
from flask_migrate import Migrate

from webargs import fields
from webargs.flaskparser import use_args

from models import db, Hotels
from serializers import ma, hotel_schema, hotels_schema

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://admin:admin@postgres:5432/booking"
db.init_app(app)
ma.init_app(app)
migrate = Migrate(app, db, compare_type=True)
api = Api(app)


class HotelView(Resource):
    def get(self, hotel_id):
        if hotel_id == 0:
            hotels = Hotels.query.all()
        else:
            hotels = Hotels.query.filter_by(id=hotel_id).first()
        return hotels_schema.dump(hotels)

    @use_args(
        {
            'name': fields.Str(required=True),
            'price': fields.Integer(required=True),
            'location': fields.Str(required=True),
            'rate': fields.Integer(required=False),
            'breakfast': fields.Boolean(required=False),
        },
        location='form'
    )
    def post(self, args, hotel_id):
        new_hotel = Hotels(**args)
        db.session.add(new_hotel)
        db.session.commit()
        return hotel_schema.dump(new_hotel)

    @use_args(
        {
            'name': fields.Str(required=False),
            'price': fields.Integer(required=False),
            'location': fields.Str(required=False),
            'rate': fields.Integer(required=False),
            'breakfast': fields.Boolean(required=False),
        },
        location='form'
    )
    def put(self, args, hotel_id):
        hotel = Hotels.query.filter_by(id=hotel_id)
        hotel.update(args)
        db.session.commit()
        return hotel_schema.dump(hotel.first())

    def delete(self, hotel_id):
        hotel = Hotels.query.filter_by(id=hotel_id).first()
        db.session.delete(hotel)
        db.session.commit()
        return {'status': 'Record was successfully deleted'}


api.add_resource(HotelView, '/api/hotels/<int:hotel_id>')

if __name__ == '__main__':
    app.run(debug=True, host='0', port=8000)
