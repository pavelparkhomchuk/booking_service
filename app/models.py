from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Hotels(db.Model):
    __tablename__ = "hotels"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    price = db.Column(db.Integer)
    location = db.Column(db.String(100))
    rate = db.Column(db.Integer)
    breakfast = db.Column(db.Boolean, nullable=True)

    def __repr__(self):
        return self.name
